// author: Mariia Kravtsova
// date: 11/11/2015
//purpose: increment value of status register by one
//         and flash light as the result

#include <msp430.h>
#include <libemb/serial/serial.h>
#include <libemb/conio/conio.h>

unsigned short int Status();

int main(void){
int count = 0;
short int retval;
short int c,v,save;

  WDTCTL  = WDTHOLD | WDTPW;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL  = CALDCO_1MHZ;

  // turn the LEDs off
  P1DIR &= ~(0b00000001);
  P1DIR &= ~(0b01000000);

	serial_init(9600);

// The check is done in a loop
for (;;)
{
  // get status from assembly file with Status function
  retval = Status();

  // turn the LEDs off
  P1DIR &= ~(0b00000001);
  P1DIR &= ~(0b01000000);

  // increment the value by one
  count = count + 1;
  save = count;
  // check if c or v are on
  if (retval & 1 == 1)
  {
    c = 1;
    // red light on
    P1DIR |= 0b00000001;
    // delay to see the lights
    __delay_cycles(100000);
  }

  else if (retval & 0x0100 == 1)
  {
    v = 1;
    // green light on
    P1DIR |= 0b01000000;
    // delay to see the lights
    __delay_cycles(100000);
  }

  // print the values of carry before the flag
  if (c == 1)
  {
    cio_printf("Value before carry: %u\n\r", save);
  }

  // print the values of overflow before the flag
  if (v == 1)
  {
    cio_printf("Value before overflow: %u\n\r", save);
  }
}

	return 0;
}
