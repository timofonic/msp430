#include <msp430.h>

int main(void) {
  WDTCTL = WDTPW | WDTHOLD;
  BCSCTL1 = CALBC1_1MHZ;
  DCOCTL = CALDC0_1MHZ;

  P1DIR = BIT0;
  P1REN = BIT3;
  P1OUT |= BIT3;
  P1IE |= BIT3;
  P1IES |= BIT3;
  P1IFG &= ~BIT3;
  _BIS_SR(LPM4_bits | GIE);
}

#pragma vector=PORT_VECTOR
__interupt void Port_1(void)
{
  P1OUT ^= BIT0;
  while (!(BIT3 & P1IN)) {}
  __delay_cycles(32000);
  P1IEG &= ~BIT3;
}
